# <img src="https://www.clipartmax.com/png/middle/258-2583618_bank-clipart-bangunan-digital-bank-icon-png.png" width=50 height=50>  Automated Bank System
<img src="https://www.lagomframework.com/images/banner-logos/lagom-full-color.svg" width=150 height=50><img src="https://www.yukiiworks.com/wp-content/uploads/2019/04/kotlin.png" width=100 height=50>
<br/>
Based on Lagom 1.6.7, wrote in java 1.8 + Kt microservice.
Demonstrates usage of Lagom framework, virtual DB (H2), synergy of old java and new kotlin.

## API
### Get list of accounts
```shell
GET /api/abs/accounts?<page-num-optional-param>
```
### Get defined account
```shell
GET /api/abs/accounts/<account-number-param>
```
### Get defined account balance
```shell
GET /api/abs/accounts/<account-number-param>/balance
```
### Get defined account paytures
```shell
GET /api/abs/accounts/<account-number-param>/paytures
```
### New payture
```shell
POST /api/abs/paytures
```
body:
```
{
  "creditAccount": {
    "account": "<account-number-param>"
    },
  "debitAccount": {
    "account": "<account-number-param>"
    },
  "currency":"RUR",
  "amount": <amount-param>
  }
```
### Get list of paytures
```shell
GET /api/abs/paytures
```

# Tech details
## DB
In-memory H2.

## Unit tests
Kotlin + jupiter.

## Run
Unix:
```shell
bash mvnw lagom:runAll
```
Windows:
```batch
mvnw.cmd lagom:runAll
```

Debug mode:
```shell
#!/bin/bash
chcp.com 1251
export MAVEN_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005"
bash mvnw clean package lagom:runAll
```
Invoking methods:
```shell

curl -X GET http://localhost:80/api/abs/accounts?pageNum=1
curl -X GET http://localhost:80/api/abs/accounts/123123123
curl -X GET http://localhost:80/api/abs/accounts/123458100001
curl -X GET http://localhost:80/api/abs/accounts/123458100001/balance
curl -X GET http://localhost:80/api/abs/accounts/123458100001/paytures
curl -X POST -d '{"creditAccount": {"number": "123458100001"}, "debitAccount": {"number": "123458100002"}, "currency":"RUR", "amount": 1}' http://localhost:80/api/abs/paytures
curl -X POST -d http://localhost:80/api/abs/paytures-fake
curl -X GET http://localhost:80/api/abs/paytures
```
## Additional
### Some links
Standalone invoke: 
https://thecoderwriter.wordpress.com/2016/09/24/run-a-lagom-service-standalone-with-zookeeper/ <br/>
https://github.com/TimMoore/lagom-mvn-standalone <br/>
https://github.com/lagom/lagom/pull/154/files  <br/>
https://www.lagomframework.com/documentation/1.0.x/java/Overview.html#deploying-using-static-service-locations
