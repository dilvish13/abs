package ru.dilvish13.abs.impl;

import static com.lightbend.lagom.javadsl.testkit.ServiceTest.defaultSetup;
import static com.lightbend.lagom.javadsl.testkit.ServiceTest.withServer;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import ru.dilvish13.abs.api.AccountBalanceData;
import ru.dilvish13.abs.api.AbsService;
import ru.dilvish13.abs.api.common.ResultData;


public class AbsServiceJTest {

  public static final String TEST_ACCOUNT_RUB_0 = "00000000000000000000";
  public static final String TEST_ACCOUNT_RUB_1 = "30101810600000000786";
  public static final String TEST_ACCOUNT_RUB_2 = "123458100001";
  public static final String TEST_ACCOUNT_USD_0 = "000000000001";
  public static final String TEST_ACCOUNT_USD_1 = "123458400001";

  @Test
  public void initialValues() {
    doInitialValues();
  }

  public static void doInitialValues() {
    withServer(defaultSetup()
            .withCassandra(false)
            .withCluster(false), server -> {
      AbsService service = server.client(AbsService.class);
      ResultData<AccountBalanceData> res0 = service.getBalance(TEST_ACCOUNT_RUB_0).invoke().toCompletableFuture().get(5, SECONDS);
      assertNotNull(res0.getMeta());
      assertEquals("NOT_FOUND", res0.getMeta().getStatus().toString());
      assertNull(res0.getData());
      ResultData<AccountBalanceData> res0u = service.getBalance(TEST_ACCOUNT_USD_0).invoke().toCompletableFuture().get(5, SECONDS);
      assertNotNull(res0u.getMeta());
      assertEquals("NOT_FOUND", res0u.getMeta().getStatus().toString());
      assertNull(res0u.getData());

      ResultData<AccountBalanceData> res1 = service.getBalance(TEST_ACCOUNT_RUB_1).invoke().toCompletableFuture().get(5, SECONDS);
      assertNotNull(res1.getMeta());
      assertEquals("OK", res1.getMeta().getStatus().toString());
      assertNotNull(res1.getData());
      assertEquals(TEST_ACCOUNT_RUB_1, res1.getData().getAccount().getNumber());

      ResultData<AccountBalanceData> res2 = service.getBalance(TEST_ACCOUNT_RUB_2).invoke().toCompletableFuture().get(5, SECONDS);
      assertNotNull(res2.getData());
      assertEquals(TEST_ACCOUNT_RUB_2, res2.getData().getAccount().getNumber());

      ResultData<AccountBalanceData> res1u = service.getBalance(TEST_ACCOUNT_USD_1).invoke().toCompletableFuture().get(5, SECONDS);
      assertNotNull(res1u.getMeta());
      assertEquals("OK", res1u.getMeta().getStatus().toString());
      assertNotNull(res1u.getData());
      assertEquals(TEST_ACCOUNT_USD_1, res1u.getData().getAccount().getNumber());
    });
  }
}
