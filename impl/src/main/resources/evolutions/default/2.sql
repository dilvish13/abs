-- next line is for play.evolutions only
-- # --- !Ups
insert into CURRENCY (ISO_CODE, DIGIT_CODE, NAME) values ('RUB', '810', 'Rubles');
insert into CURRENCY (ISO_CODE, DIGIT_CODE, NAME) values ('USD', '840', 'US Dollars');
--
insert into ACCOUNT (NUM, CURRENCY_ISO_CODE, NAME) values ('30101810200000000593', 'RUB', 'Sberbank');
insert into ACCOUNT (NUM, CURRENCY_ISO_CODE, NAME) values ('30101810400000000225', 'RUB', 'Alphabank');
insert into ACCOUNT (NUM, CURRENCY_ISO_CODE, NAME) values ('30101810300000000985', 'RUB', 'Alphabank');
insert into ACCOUNT (NUM, CURRENCY_ISO_CODE, NAME) values ('30101810600000000786', 'RUB', 'VTB');
insert into ACCOUNT (NUM, CURRENCY_ISO_CODE, NAME) values ('30101810145250000411', 'RUB', 'VTB');
insert into ACCOUNT (NUM, CURRENCY_ISO_CODE, NAME) values ('123458100001', 'RUB', 'test rub 1');
insert into ACCOUNT (NUM, CURRENCY_ISO_CODE, NAME) values ('123458100002', 'RUB', 'test rub 2');
insert into ACCOUNT (NUM, CURRENCY_ISO_CODE, NAME) values ('123458400001', 'USD', 'test usd 1');
insert into ACCOUNT (NUM, CURRENCY_ISO_CODE, NAME) values ('123458400002', 'USD', 'test usd 12');
--
insert into PAYTURE (ID, ACCOUNT_FROM_NUM, ACCOUNT_TO_NUM, AMOUNT, CREATE_DATE, DESCRIPTION)
             values (-10000001, '000008100001', '123458100001', 100, sysdate-1, 'fake payture 1');
insert into PAYTURE (ID, ACCOUNT_FROM_NUM, ACCOUNT_TO_NUM, AMOUNT, CREATE_DATE, DESCRIPTION)
             values (-10000002, '000008100001', '123458100002', 50, sysdate-1, 'fake payture 2');
insert into PAYTURE (ID, ACCOUNT_FROM_NUM, ACCOUNT_TO_NUM, AMOUNT, CREATE_DATE, DESCRIPTION)
             values (-10000003, '000008100001', '123458400001', 200, sysdate-2, 'fake payture 3');
insert into PAYTURE (ID, ACCOUNT_FROM_NUM, ACCOUNT_TO_NUM, AMOUNT, CREATE_DATE, DESCRIPTION)
             values (-10000004, '000008100001', '123458400002', 400, sysdate-3, 'fake payture 4');
insert into PAYTURE (ID, ACCOUNT_FROM_NUM, ACCOUNT_TO_NUM, AMOUNT, CREATE_DATE, DESCRIPTION)
             values (-10000005, '123458100001', '123458100002', 5, sysdate, 'fake payture 5');
-- next line is for play.evolutions only
--# --- !Downs
--TRUNCATE TABLE PAYTURE;
--TRUNCATE TABLE ACCOUNT;
--TRUNCATE TABLE CURRENCY;