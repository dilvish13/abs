package ru.dilvish13.abs;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigBeanFactory;
import ru.dilvish13.abs.api.AbsService;
import ru.dilvish13.abs.application.AbsServiceImpl;

/**
 * The module that binds the Service so that it can be served.
 * For k8s or dev mode. For standalone use another module.
 */
public class AbsModule extends AbstractModule implements ServiceGuiceSupport {
  @Override
  protected void configure() {
    bindService(AbsService.class, AbsServiceImpl.class);
    bind(AccountRepository.class).asEagerSingleton();
  }

  /**
   * Конфигурация.
   *
   * @param config {@link Config}
   * @return {@link Configuration}
   */
  @Provides
  public Configuration provideConfiguration(Config config) {
    return ConfigBeanFactory.create(config.getConfig("abs"), Configuration.class);
  }

}
