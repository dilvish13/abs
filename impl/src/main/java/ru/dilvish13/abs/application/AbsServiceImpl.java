package ru.dilvish13.abs.application;

import akka.NotUsed;
import com.lightbend.lagom.javadsl.api.ServiceCall;

import org.apache.commons.lang3.tuple.Pair;
import org.hibernate.exception.SQLGrammarException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.dilvish13.abs.api.AccountBalanceData;
import ru.dilvish13.abs.api.AccountData;
import ru.dilvish13.abs.api.AccountsData;
import ru.dilvish13.abs.api.PaytureData;
import ru.dilvish13.abs.api.AbsService;
import ru.dilvish13.abs.AccountRepository;
import ru.dilvish13.abs.Configuration;
import ru.dilvish13.abs.api.common.GenericStatus;
import ru.dilvish13.abs.api.common.ResultData;
import ru.dilvish13.abs.data.Account;
import ru.dilvish13.abs.data.Payture;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static java.lang.String.format;
import static java.lang.String.join;
import static java.math.BigDecimal.ONE;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.*;
import static ru.dilvish13.abs.api.PaytureData.PaytureDirection.CREDIT;
import static ru.dilvish13.abs.api.PaytureData.PaytureDirection.DEBIT;
import static ru.dilvish13.abs.data.ConvertersKt.toAccountData;
import static ru.dilvish13.abs.data.ConvertersKt.toPaytureData;

/**
 * Implementation of the AbsService.
 */
public class AbsServiceImpl implements AbsService {

    private final Configuration configuration;
    private final AccountRepository repository;
    private static final Logger logger;

    static {
        logger = LoggerFactory.getLogger(AbsServiceImpl.class);
    }

    @Inject
    public AbsServiceImpl(Configuration configuration, AccountRepository accountRepository) {
        this.configuration = configuration;
        this.repository = accountRepository;
    }

    @Override
    public ServiceCall<NotUsed, ResultData<AccountsData>> getAccounts(Optional<Integer> pageNum) {
        logger.info("getAccounts");
        return notUsed -> repository
                .getAccounts()
                .thenApply(list -> AccountsData.builder()
                        .accounts(list.stream().map(a -> toAccountData(a, configuration.getSwift())).collect(toList()))
                        .pageNum(1)
                        .pageCount(1)
                        .build())
                .thenApply(ResultData::ok);
    }

    @Override
    public ServiceCall<NotUsed, ResultData<AccountData>> getAccount(String number) {
        logger.info("getAccount");
        return notUsed -> repository.getAccount(number)
                .thenApply(acc -> {
                    if (acc == null) {
                        throw new CheckException(format("Account %s not found", number));
                    }
                    return acc;
                })
                .thenApply(a -> toAccountData(a, configuration.getSwift()))
                .handle( (r, ex) -> {
                    if (ex != null) {
                        return ResultData.of(new GenericStatus("NOT_FOUND"), "error.unknown.account", ex.getMessage());
                    } else {
                        return ResultData.ok(r);
                    }
                })
                ;
    }

    @Override
    public ServiceCall<NotUsed, ResultData<AccountBalanceData>> getBalance(String number) {
        return notUsed -> getBalanceData(number)
                .handle((r, ex) -> {
                    if (ex != null) {
                        return ResultData.of(new GenericStatus("NOT_FOUND"), "error.unknown.account", ex.getMessage());
                    } else {
                        return ResultData.ok(r);
                    }
                });
    }

    public CompletionStage<AccountBalanceData> getBalanceData(String number) {
        logger.info("getBalance");
        return repository.getAccountEx(number)
                .thenApply(acc -> {
                    if (acc == null) {
                        throw new CheckException(format("Account %s not found", number));
                    }
                    return acc;
                })
                .thenApply(acc -> Pair.of(acc,acc.turnoverCr() - acc.turnoverDb()))
                .thenApply(pair -> AccountBalanceData.builder()
                        .account(toAccountData(pair.getLeft(), configuration.getSwift()))
                        .amount(BigDecimal.valueOf(pair.getRight()).setScale(2, RoundingMode.HALF_UP))
                        .build()
                );
    }

    @Override
    public ServiceCall<NotUsed, ResultData<List<PaytureData>>> getPaytures(String number) {
        logger.info("getPaytures");
        return notUsed -> repository.getAccountEx(number)
                .thenApply(acc -> { if (acc == null) throw new CheckException(format("Account %s not found.", number)); return acc; })
                .thenApply(acc -> {
                    List<PaytureData> result = new ArrayList<>();
                    if (acc.getCreditPaytures() != null) {
                        result.addAll(acc.getCreditPaytures()
                                .stream()
                                .filter(p -> p.getDeclineDate() == null)
                                .map(p -> toPaytureData(p, CREDIT, configuration.getSwift()))
                                .collect(toList()));
                    }
                    if (acc.getDebitPaytures() != null) {
                        result.addAll(acc.getDebitPaytures()
                                .stream()
                                .filter(p -> p.getDeclineDate() == null)
                                .map(p -> toPaytureData(p, DEBIT, configuration.getSwift()))
                                .collect(toList()));
                    }
                    return result.stream()
                            .sorted(PaytureData.COMPARATOR_BY_DATE)
                            .collect(toList());
                })
                .handle((r, ex) -> { if (ex != null) {
                    return ResultData.of(GenericStatus.getNOT_FOUND(), "error.unknown.account", ex.getMessage());
                } else {
                    return ResultData.ok(r);
                }});
    }

    @Override
    public ServiceCall<NotUsed, ResultData<List<PaytureData>>> getAllPaytures() {
        logger.info("getBalance");
        return notUsed -> repository.getAllPaytures().thenApply(ResultData::ok);
    }

    @Override
    public ServiceCall<PaytureData, ResultData<?>> doInnerPayture() {
        logger.info("doInnerPayture");
        return paytureData -> {
            // todo: different currency check
            try {
                final Account accDb = repository.getAccount(paytureData.getDebitAccount().getNumber()).toCompletableFuture().join();
                final BigDecimal balanceDb = getBalanceData(requireNonNull(accDb.getNumber())).toCompletableFuture().join().getAmount();
                if (balanceDb.compareTo(paytureData.getAmount()) < 0) {
                    throw new CheckException("No money for transaction.");
                }
                final Account accCr = repository.getAccount(paytureData.getCreditAccount().getNumber()).toCompletableFuture().join();
                repository.insertPayture(buildInnerPayture(accDb, accCr, paytureData));
                return CompletableFuture.completedFuture(ResultData.ok());
            } catch (CheckException e) {
                logger.error("Transaction check {}.", e.getMessage());
                return CompletableFuture.completedFuture(ResultData.error("error.transaction.check", e.getMessage()));
            } catch (SQLGrammarException e) {
                logger.error("Transaction DB error (SQL: {}, Error code ORA-{}, SQLException: {}): {}.", e.getSQL(), e.getSQLException().getErrorCode(), e.getSQLException().getMessage(), e.getMessage());
                return CompletableFuture.completedFuture(ResultData.error("error.unknown", Arrays.stream(e.getStackTrace()).map(Object::toString).collect(joining("\n"))));
            } catch (Exception e) {
                logger.error("Transaction unknown error {}.", e.getMessage());
                return CompletableFuture.completedFuture(ResultData.error("error.unknown", Arrays.stream(e.getStackTrace()).map(Object::toString).collect(joining("\n"))));
            }
        };
    }

    private Payture buildInnerPayture(Account accDb, Account accCr, PaytureData paytureData) {
        if (accCr == null) throw new CheckException("Invalid credit account (not found)");
        if (accDb == null) throw new CheckException("Invalid debit account (not found)");
        if (accCr.getCurrency() == null) throw new CheckException("Invalid credit account (currency not found)");
        if (accDb.getCurrency() == null) throw new CheckException("Invalid debit account (currency not found)");
        if (!accDb.getCurrency().equals(accCr.getCurrency())) throw new CheckException("Invalid payture (different currencies)");
        return Payture.simple(
                accDb.getNumber(),
                accCr.getNumber(),
                ONE,
                paytureData.getAmount(),
                LocalDateTime.now(),
                paytureData.getDescription() != null
                        ? paytureData.getDescription()
                        : "Payture from api"
        );
    }

    @Override
    public ServiceCall<NotUsed, ResultData<PaytureData>> doFakePayture() {
        return notUsed -> repository.insertFake().thenApply(p -> ResultData.ok(toPaytureData(p, null, null)));
    }

  public static class CheckException extends RuntimeException {
      public CheckException(String message) {
          super(message);
      }
  }
}
