package ru.dilvish13.abs

import org.taymyr.play.repository.infrastructure.persistence.DatabaseExecutionContext
import org.taymyr.play.repository.infrastructure.persistence.JPARepository
import play.db.jpa.JPAApi
import ru.dilvish13.abs.api.PaytureData
import ru.dilvish13.abs.data.Account
import ru.dilvish13.abs.data.Payture
import ru.dilvish13.abs.data.toPaytureData
import java.math.BigDecimal
import java.math.BigDecimal.ONE
import java.math.BigDecimal.ZERO
import java.time.LocalDateTime
import java.util.concurrent.CompletionStage
import javax.inject.Inject
import javax.inject.Singleton
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root

@Singleton
class AccountRepository @Inject constructor(
    jpaApi: JPAApi,
    executionContext: DatabaseExecutionContext,
    private val configuration: Configuration,
): JPARepository<Account, Int>(jpaApi, executionContext, Account::class.java) {

    fun getAccount(number: String): CompletionStage<Account?> =
        execute{ it.find(Account::class.java, number) }

    fun getAccountEx(number: String): CompletionStage<Account?> =
        execute{ em -> em.find(Account::class.java, number)
            .also { it?.creditPaytures?.iterator() }
            .also { it?.debitPaytures?.iterator() }}

    fun getAccounts(): CompletionStage<List<Account>> =
        execute { em ->
            em.createQuery(em.criteriaBuilder
                .createQuery(Account::class.java)
                .let { it to it.from(Account::class.java) }
                .let { it.first.select(it.second) }
            ).resultList
        }

    fun getAllPaytures(): CompletionStage<List<PaytureData>?> =
        execute { em ->
            em.createQuery(em.criteriaBuilder
                .createQuery(Account::class.java)
                .let { it to it.from(Account::class.java) }
                .let { it.first.select(it.second) }
            ).resultList
                .map { account ->
                    (account.creditPaytures ?: listOf())
                        .map { toPaytureData(it, null, configuration.swift) }
                        .toMutableList()
                        .also {
                            it.addAll(
                                (account.debitPaytures ?: listOf())
                                    .map { toPaytureData(it, null, configuration.swift) }
                            )
                        }
                }
                .reduce { aggregated, list -> aggregated.also { it += list } }
                .sortedWith(PaytureData.COMPARATOR_BY_DATE)
                .distinct()
                .toList()
        }

    fun insertPayture(payture: Payture): CompletionStage<Payture> =
        execute{ em -> payture.also { em.persist(it) } }

    fun insertFake() = insertPayture(
        Payture(
            accountFromNum = "30101810200000000593",
            accountToNum = "30101810400000000225",
            accountFromSwift = configuration.swift,
            accountToSwift = configuration.swift,
            currencyRate = ONE,
            amount = ONE,
            createDate = LocalDateTime.now(),
            description = "fake",
        )
    )

    override fun nextIdentity(): Int {
        throw Exception("Not implemented")
    }
}
