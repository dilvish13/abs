package ru.dilvish13.abs.data

import java.math.BigDecimal
import java.util.function.BinaryOperator
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "ACCOUNT")
data class Account @JvmOverloads constructor(
    @Id
    @Column(name = "NUM")
    var number: String? = null,
    @ManyToOne
    @JoinColumn(name = "CURRENCY_ISO_CODE")
    var currency: Currency? = null,
    @OneToMany(cascade = [CascadeType.ALL])
    @JoinColumn(name = "account_to_num")
    var creditPaytures: MutableList<Payture>? = mutableListOf(),
    @OneToMany(cascade = [CascadeType.ALL])
    @JoinColumn(name = "account_from_num")
    var debitPaytures: MutableList<Payture>? = mutableListOf(),
) {

    fun turnoverDb() = debitPaytures?.turnover() ?: 0

    fun turnoverCr() = creditPaytures?.turnover() ?: 0

    override fun hashCode(): Int {
        return number.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return this === other || (other is Account && this.number.equals(other.number))
    }

    override fun toString(): String {
        return "${this.javaClass.simpleName}($number, ${currency?.isoCode ?: "<null>"}"
    }
}

fun List<Payture>.turnover() = this.sumBy { it.amount?.toInt() ?: 0 }