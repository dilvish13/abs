package ru.dilvish13.abs.data

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "CURRENCY")
data class Currency (
    @Id
    @Column(name = "ISO_CODE")
    var isoCode: String? = null,
    @Column(name = "DIGIT_CODE")
    var code: String? = null,
    @Column(name = "NAME")
    var name: String? = null,
) {
    override fun equals(other: Any?): Boolean {
        return this === other ||
                (other is Currency && isoCode.equals(other.isoCode) && code.equals(other.code))
    }

    override fun hashCode(): Int {
        return isoCode.hashCode()
    }
}