package ru.dilvish13.abs.data

import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import kotlinx.serialization.json.Json
import kotlinx.serialization.encodeToString

@Entity
@Table(name = "PAYTURE")
class Payture @JvmOverloads constructor(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "payture_id_generator")
    @SequenceGenerator(name="payture_id_generator", sequenceName = "PAYTURE_ID_SEQ", allocationSize=50)
    var id: Long? = null,
    @Column(name ="account_from_num")
    var accountFromNum: String? = null,
    @Column(name ="account_from_swift")
    var accountFromSwift: String? = null,
    @Column(name ="account_to_num")
    var accountToNum : String? = null,
    @Column(name ="account_to_swift")
    var accountToSwift : String? = null,
    @Column(name ="amount")
    var amount: BigDecimal? = null,
    @Column(name ="currency_rate")
    var currencyRate: BigDecimal? = null,
    @Column(name ="create_date")
    var createDate: LocalDateTime? = null,
    @Column(name ="accept_date")
    var acceptDate: LocalDateTime? = null,
    @Column(name ="decline_date")
    var declineDate: LocalDateTime? = null,
    @Column(name ="description")
    var description: String? = null
) {
    override fun hashCode(): Int = (id ?: toString()).hashCode()

    override fun toString(): String = Json.encodeToString(this)

    override fun equals(other: Any?): Boolean =
        other is Payture && this.id != null && this.id == other.id

    companion object {
        @JvmStatic
        fun simple(
            accountFromNum: String?,
            accountToNum: String?,
            currencyRate: BigDecimal?,
            amount: BigDecimal?,
            createDate: LocalDateTime?,
            description: String?,
        ) = Payture(
            accountFromNum = accountFromNum,
            accountToNum = accountToNum,
            currencyRate = currencyRate,
            amount = amount,
            createDate = createDate,
            description = description,
        )
    }
}
