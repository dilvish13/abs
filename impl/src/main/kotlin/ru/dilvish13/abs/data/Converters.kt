package ru.dilvish13.abs.data

import ru.dilvish13.abs.api.AccountData
import ru.dilvish13.abs.api.PaytureData

fun toAccountData(account: Account, swift: String? = null): AccountData =
        AccountData.builder()
            .number(account.number)
            .currency(account.currency?.isoCode ?: "<null>")
            .swift(swift)
            .build()

fun toPaytureData(payture: Payture, direction: PaytureData.PaytureDirection?, swift: String? = null): PaytureData =
    PaytureData.builder()
        .paytureId(payture.id)
        .debitAccount(AccountData(
            payture.accountFromNum,
            "<N/A>",
            swift
        ))
        .creditAccount(AccountData(
            payture.accountToNum,
            "<N/A>",
            swift
        ))
        .currency("<N/A>")
        .amount(payture.amount?.setScale(2))
        .createDate(payture.createDate)
        .description(payture.description)
        .createDate(payture.createDate)
        .direction(direction)
        .build()
