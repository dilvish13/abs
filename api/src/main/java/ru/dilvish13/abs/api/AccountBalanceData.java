package ru.dilvish13.abs.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

@Data
@Builder
@JsonInclude(NON_EMPTY)
@AllArgsConstructor(onConstructor_ = {@JsonCreator})
public class AccountBalanceData {
  AccountData account;
  BigDecimal amount;
}
