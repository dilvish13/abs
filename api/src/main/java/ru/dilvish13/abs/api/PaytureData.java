package ru.dilvish13.abs.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Comparator;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static java.util.Comparator.comparing;

@Value
@Builder
@JsonDeserialize
@JsonInclude(NON_EMPTY)
@AllArgsConstructor(onConstructor_ = {@JsonCreator})
public class PaytureData {
  @JsonIgnore
  public static final Comparator<PaytureData> COMPARATOR_BY_DATE =
          comparing(PaytureData::getCreateDate)
                  .thenComparing(PaytureData::getDirection, PaytureDirection.CREDIT_FIRST)
                  .thenComparing(PaytureData::getAmount);

  // out param
  Long paytureId;
  AccountData creditAccount;
  AccountData debitAccount;
  BigDecimal amount;
  String currency;
  String description;
  // out param
  LocalDateTime createDate;
  // out param
  PaytureDirection direction;

  public enum PaytureDirection {
    CREDIT, DEBIT;

    public static final Comparator<PaytureDirection> CREDIT_FIRST
            = (d1, d2) -> (d2 == CREDIT ? 0 : 1) - (d1 == CREDIT ? 0 : 1);
  }
}
