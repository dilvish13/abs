package ru.dilvish13.abs.api.common

import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize

interface Status

@JsonSerialize(using = GenericStatus.Serializer::class)
@JsonDeserialize(using = GenericStatus.Deserializer::class)
data class GenericStatus(val value: String) : Status {

    override fun toString(): String = value

    class Serializer : JsonSerializer<GenericStatus>() {
        override fun serialize(status: GenericStatus?, gen: JsonGenerator?, serializers: SerializerProvider?) {
            gen?.run { status?.let { writeString(it.value) } }
        }
    }

    class Deserializer : JsonDeserializer<GenericStatus>() {
        override fun deserialize(parser: JsonParser?, ctx: DeserializationContext?): GenericStatus =
            GenericStatus(parser?.text.orEmpty())
    }

    companion object {
        @JvmStatic
        fun of(value: String): Status = GenericStatus(value)

        @JvmStatic
        val OK = GenericStatus("OK")

        @JvmStatic
        val ERROR = GenericStatus("ERROR")

        @JvmStatic
        val BAD_REQUEST = GenericStatus("BAD_REQUEST")

        @JvmStatic
        val UNAUTHORIZED = GenericStatus("UNAUTHORIZED")

        @JvmStatic
        val FORBIDDEN = GenericStatus("FORBIDDEN")

        @JvmStatic
        val NOT_FOUND = GenericStatus("NOT_FOUND")

        @JvmStatic
        val TOO_MANY_REQUESTS = GenericStatus("TOO_MANY_REQUESTS")

        @JvmStatic
        val SERVICE_UNAVAILABLE = GenericStatus("SERVICE_UNAVAILABLE")
    }
}
