package ru.dilvish13.abs.api.common

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL
import com.fasterxml.jackson.databind.annotation.JsonDeserialize

@JsonInclude(NON_NULL)
@JsonDeserialize
class ResultData<T> @JvmOverloads constructor(
    val meta: Meta,
    val data: T? = null
) {

    @JvmOverloads
    constructor(status: Status, message: String? = null, data: T? = null) : this(Meta(status, message), data)

    @JsonCreator
    // Jackson demands that trick for property-based constructors
    constructor(meta: Meta, data: T?, dummy: String?) : this(meta, data)

    companion object {
        @JvmStatic
        @JvmOverloads
        fun <T> of(meta: Meta, data: T? = null) = ResultData(meta, data)

        @JvmStatic
        @JvmOverloads
        fun <T> of(status: Status, message: String? = null, description: String? = null, data: T? = null) =
            ResultData(Meta(status, message, description), data)

        @JvmStatic
        @JvmOverloads
        fun <T> ok(data: T? = null) = ResultData(Meta.ok(), data)

        @JvmStatic
        @JvmOverloads
        fun <T> ok(message: String, description: String? = null, data: T? = null) = ResultData(Meta.ok(message, description), data)

        @JvmStatic
        @JvmOverloads
        fun <T> error(data: T? = null) = ResultData(Meta.error(), data)

        @JvmStatic
        @JvmOverloads
        fun <T> error(message: String, description: String? = null, data: T? = null) = ResultData(Meta.error(message, description), data)
    }

    @JsonInclude(NON_NULL)
    data class Meta @JvmOverloads constructor(
        @field:JsonDeserialize(`as` = GenericStatus::class)
        val status: Status,
        val message: String? = null,
        val description: String? = null
    ) {

        @JsonCreator
        // Jackson trick
        constructor(
            status: Status,
            message: String?,
            description: String?,
            dummy: String?
        ) : this(status, message, description)

        companion object {
            @JvmStatic
            @JvmOverloads
            fun of(status: Status, message: String? = null, description: String? = null): Meta = Meta(status, message, description)

            @JvmStatic
            @JvmOverloads
            fun of(status: String, message: String? = null, description: String? = null): Meta =
                Meta(GenericStatus(status), message, description)

            @JvmStatic
            @JvmOverloads
            fun ok(message: String? = null, description: String? = null) = Meta(GenericStatus.OK, message, description)

            @JvmStatic
            @JvmOverloads
            fun error(message: String? = null, description: String? = null) = Meta(GenericStatus.ERROR, message, description)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ResultData<*>) return false

        if (meta != other.meta) return false
        if (data != other.data) return false

        return true
    }

    override fun hashCode(): Int {
        var result = meta.hashCode()
        result = 31 * result + (data?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "ResultData(meta=$meta, data=$data)"
    }
}
